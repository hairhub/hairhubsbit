<?php 
    session_start();
    include('connection.php'); 
    $username= $_SESSION['userId'];
?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>HairHub| Update Profile</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php 
        $errors = array();
        include('errors.php');
        if (isset($_POST['update'])){
            $fname = $_POST['fname'];
            $email = $_POST['email'];
            $phone =  $_POST['phone'];
            $address =  $_POST['address'];
            $pass = $_POST['password'];
            $pass1 = $_POST['password1'];
            $pass2 = $_POST['password2'];

            if ($pass1 != $pass2)
            {
                array_push($errors, '<script>window.alert("The two passwords do not match");</script>');
            }
            
            if (count($errors) == 0)
            {
            $sql = "UPDATE customer_register SET fullname = '$fname' , email_address = '$email', phone_number = '$phone', home_address = '$address',password = '$pass2' WHERE username = '$username'";
            if (mysqli_query($conn, $sql)) {
                    
                    echo '<script>window.alert("Thanks for Updating your profile");</script>';
                } 
                else {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }  
                header('Location: view_user_profile.php');
            }
        }
        
        function test_input($data)
        {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
        }
        ?>
        
    </head>
    <!-- End of Head -->
    
    
    <body>
        <ol class="breadcrumb" style="background-color: #343a40; color: white;">
            <li class="breadcrumb-item">View Profile</li>
            <li class="breadcrumb-item active">Update Profile</li>
        </ol>
        
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-12">
                    <h3 style="margin-left:20px;margin-bottom: 10px;"><b>UPDATE PROFILE</b></h3>
                    <hr style="background-color:red;"/>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                        <?php include('errors.php'); ?>
                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="fname"><i class="fa fa-user" style="font-size: 120%;"><b> &nbsp;Full Name: </b></i></label>
                            <input type="text" name="fname" class="form-control" id="fname" required="" />
                        </div>

                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="email"><i class="fa fa-envelope" style="font-size: 120%;"><b> &nbsp;Email Address: </b></i></label>
                            <input type="email" class="form-control" id="email" name="email" required="">
                        </div>

                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="phone"><i class="fa fa-phone" style="font-size: 120%;"><b> &nbsp;Phone Number: </b></i></label>
                            <input type="tel" class="form-control" id="phone" name="phone" required="">
                        </div>

                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="address"><i class="fa fa-home" style="font-size: 120%;"><b> &nbsp;Home Address: </b></i></label>
                            <textarea rows="5"  name="address" id="address" required="" class="form-control" required=""></textarea>
                        </div>

                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="password"><i class="fa fa-lock" style="font-size: 120%;"><b> &nbsp; Old Password: </b></i></label>
                            <input type="password" class="form-control" id="password" name="password" required="">
                        </div>
                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="password1"><i class="fa fa-lock" style="font-size: 120%;"><b> &nbsp;New Password: </b></i></label>
                            <input type="password" class="form-control" id="password1" name="password1" required=""/>
                        </div>   
                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="password2"><i class="fa fa-lock" style="font-size: 120%;"><b> &nbsp;New Password: </b></i></label>
                            <input type="password" class="form-control" id="password2" name="password2" required=""/>
                        </div>  
                        
                        <button type="submit" name="update" class="btn btn-primary" value="update" style="margin-top:5px;margin-bottom: 15px;margin-left: 40px;"><b>UPDATE</b></button>
                        
                         </form>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12 text-center">
                                <button class="btn btn-danger" style="margin-top:5px;margin-bottom: 15px;margin-left: 140px;"><a href="view_user_profile.php" style="color: white;"><b>GO BACK</b></a></button>
                                
                            </div>  
                        </div>
                   
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <img src="img/186703_12827908_2441458_a2a821eb_image.jpg" width="100%"/>
                </div>
            </div>
        </div>
        
        
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      
    </body>
</html>