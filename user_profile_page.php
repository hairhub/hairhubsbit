<?php 
    session_start(); //To start the session of the program
    include('connection.php');
?>
<!DOCTYPE html>

<html>

    <head>

        <meta charset="UTF-8"> 
        <!-- character set -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- To support Microsoft Internet Edge Browser-->
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <!--To support mobile by adjusting to screen size and scaling the zoom to 1 -->
        
        <title>HairHub | User Profile </title>
        <!-- Title of the Web site -->
        
        <!-- To link the Bootstrap and the Font Awesome style sheets to the page-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   
        <!-- To support lower versions of the Internet Explorer -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->  
    </head>

    <body style="background-image: url('img/bg1.jpg'); background-repeat: no-repeat;background-size: cover;">
        <header>
            <nav class="navbar navbar-expand-sm navbar-dark" style="background-color:lightcoral;opacity: 0.8;">
                <a class="navbar-brand" href="index.php">
                    <img src="img/imageedit_3_5270320318.png" alt="Logo" style="width:70px;" >
                    
                </a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a  class="nav-link" href="#" style="color: white;">Welcome , <strong><?php echo $_SESSION['userId']." | "; $username= $_SESSION['userId']; ?></strong></a>
                        </li>
                        <li class="nav-item">
                            <?php if(isset($_SESSION['userId'])) : ?>
                            <a class="nav-link" href="index.php?logout='1'" style="color:red;">Logout</a>
                            <?php endif ?>
                        </li>     
                    </ul>
                </div><!-- End of the Navigation Tabs -->
            </nav>
        <!-- End of Navigation Links -->
        </header>
        <!-- End of the header part -->

        <div id="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2 col-12">
                        <div class="list-group" style="margin-left:-15px;">
                            <li class="list-group-item" style="background-color: #c99695;width: 100%;color: white;font-size: 150%;"><b>MY PROFILE</b></li>
                            <a href="view_user_profile.php" class="list-group-item" target="profile" style="color:darkslategrey ;background-color: #c99695;width: 100%;"><i class="fa fa-user" style="font-size: 100%;">&nbsp;&nbsp;&nbsp;<u><b>VIEW PROFILE</b></u></i></a>
                            <a href="product_page.php" class="list-group-item" target="profile" style="color: darkslategrey;background-color: #c99695;width: 100%;"><i class="fa fa-product-hunt" style="font-size: 100%;">&nbsp;&nbsp;&nbsp;<u><b>PRODUCTS</b></u></i></a>
                            <a href="view_cart.php" class="list-group-item" target="profile" style="color: darkslategrey;background-color: #c99695;width: 100%;"><i class="fa fa-shopping-cart" style="font-size: 100%;">&nbsp;&nbsp;&nbsp;<u><b>SHOPPING CART</b></u></i></a>
                            <a href="checkout.php" class="list-group-item" target="profile" style="color: darkslategrey;background-color: #c99695;width: 100%;"><i class="fa fa-money" style="font-size: 100%;">&nbsp;&nbsp;&nbsp;<u><b>CHECKOUT</b></u></i></a>
                            <li class="list-group-item" style="background-color: #c99695;width: 100%;color: white;font-size: 150%;height: 424px;"></li>
                        </div>
                    </div>
                    <div class="col-sm-10 col-12">
                        <div class="container-fluid" style="margin-left:-45px;width:106%;">
                            <iframe name="profile"  scrolling="yes" style="width: 100%;height:681px;background-image: url('img/attractive-bag-beautiful-923210.jpg'); background-repeat: no-repeat;background-size: cover;" ></iframe>
                            
                        </div>
                    </div>
                    
                </div>
                
            </div>
            <hr style="background-color:maroon;margin-top: -6px;"/>
        </div>
        
        <footer>
            <div class="container-fluid" style="margin-top:25px;">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <p><img src="img/imageedit_3_5270320318.png" width="10%"/></p>
                        
                        <i class="fa fa-facebook-official" style="font-size: 200%;color: blue;"></i>
                        <i class="fa fa-twitter-square" style="font-size: 200%;color: lightskyblue;"></i>
                        <i class="fa fa-google-plus-official" style="font-size: 200%;color: red;"></i>
                        
                        <p><b>Copyright &COPY; 2018. All Right Reserved.</b></p>
                    </div>
                </div>
            </div>
        </footer>
        
        <!-- Javascripts Added to the web page -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      
    </body>
</html>
