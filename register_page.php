<?php 
    session_start(); //To start the session of the program
    include('connection.php');
    include('register_php.php');
?>

<!DOCTYPE html>

<html>

    <head>

        <meta charset="UTF-8"> 
        <!-- character set -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- To support Microsoft Internet Edge Browser-->
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <!--To support mobile by adjusting to screen size and scaling the zoom to 1 -->
        
        <title>HairHub | Register </title>
        <!-- Title of the Web site -->
        
        <!-- To link the Bootstrap and the Font Awesome style sheets to the page-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   
        <!-- To support lower versions of the Internet Explorer -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->  
    </head>

    <style>
        .parallax {
            /* The image used */
            background-image: url("img/brunette-female-girl-206296.jpg");

            /* Set a specific height */
            height: 900px; 

            /* Create the parallax scrolling effect */
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            }
            /* Turn off parallax scrolling for tablets and phones. Increase/decrease the pixels if needed */
            @media only screen and (max-device-width: 1024px) {
                .parallax {
                    background-attachment: scroll;
                }


            }
    </style>

    <body>
        <header>
            <nav class="navbar navbar-expand-sm navbar-dark fixed-top" style="background-color:lightcoral;opacity: 0.8;">
                <a class="navbar-brand" href="index.php">
                    <img src="img/imageedit_3_5270320318.png" alt="Logo" style="width:70px;">     
                </a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="collapsibleNavbar" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" style="color:white;" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white;" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" style="color:white;" href="#">Products</a>
                        </li>   
                        <li class="nav-item">
                            <a class="nav-link" style="color:white;" href="#">Contact</a>
                        </li>
                    </ul>
                </div><!-- End of the Navigation Tabs -->

                <!--Social media navbar links -->
                <div class="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="color:blue;font-size: 150%;"><i class="fa fa-facebook"></i></a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="color:lightskyblue;font-size: 150%;"><i class="fa fa-twitter"></i></a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="color:red;font-size: 150%;"><i class="fa fa-google-plus"></i></a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" style="color:white;font-size: 18px;" href="index.php">| Login</a>
                        </li>
                    </ul>
                </div><!-- End of the Social Media Navigation Tabs -->
            </nav>
        </header>
        <!-- End of the header part -->
        
        <div id="main-content">
            <div class="parallax">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4" style="background-color:#c99695;opacity: 0.9;margin-top: 100px;border: 1px solid #c99695;border-radius: 20px;margin-left: 10px;">
                        <div class="container" style="">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 style="margin-top:10px;">REGISTER</h4>
                                    <hr style="background-color:red;"/>
                                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                                    <?php include('errors.php'); ?>

                                    <div class="form-group input-group-sm">
                                        <label for="fname"><i class="fa fa-user" style="font-size: 120%;"><b> &nbsp;Full Name: </b></i></label>
                                        <input type="text" name="fname" class="form-control form-control-sm" id="fname" required="" />
                                    </div>

                                    <div class="form-group input-group-sm">
                                        <label for="uname"><i class="fa fa-user" style="font-size: 120%;"> <b>&nbsp;Username: </b></i></label>
                                        <input type="text" name="uname" class="form-control form-control-sm" id="uname" required="" />
                                    </div>

                                    <div class="form-group input-group-sm">
                                        <label for="email"><i class="fa fa-envelope" style="font-size: 120%;"> <b>&nbsp;Email Address: </b></i></label>
                                        <input type="email" class="form-control" id="email" name="email" required="">
                                    </div>

                                    <div class="form-group input-group-sm">
                                        <label for="phone"><i class="fa fa-phone" style="font-size: 120%;"> <b>&nbsp;Phone Number: </b></i></label>
                                        <input type="tel" class="form-control" id="phone" name="phone" required="">
                                    </div>

                                    <div class="form-group input-group-sm">
                                        <label for="address"><i class="fa fa-home" style="font-size: 120%;"><b>&nbsp;Home Address: </b></i></label>
                                        <textarea rows="2"  name="address" id="address" placeholder="Address" required="" class="form-control" required=""></textarea>
                                    </div>

                                    <div class="form-group input-group-sm">
                                        <label for="password"><i class="fa fa-lock" style="font-size: 120%;"><b> &nbsp;Password: </b></i></label>
                                        <input type="password" class="form-control" id="password" name="password" required="">
                                    </div>

                                    <div class="form-group input-group-sm">
                                        <label for="password2"><i class="fa fa-lock" style="font-size: 120%;"><b> &nbsp;Confirm Password: </b></i></label>
                                        <input type="password" class="form-control" id="password2" name="password2" required=""/>
                                    </div>
                                        
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value="" required="">Agree to Terms and Conditions
                                        </label>
                                    </div>

                                        <button type="submit" name="register" class="btn btn-secondary" value="register" style="margin-top:5px;"><b>REGISTER</b></button>
                                    </form>
                                    <p style="text-align:left;margin-top: 15px;">Already have an account?&nbsp;<a href="index.php"><b>LOGIN HERE</a></b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8"></div>
                </div>
            </div>
            </div>  
        </div>
        
        
        <footer>
            <div class="container-fluid" style="margin-top:25px;">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <p><img src="img/imageedit_3_5270320318.png" width="10%"/></p>
                        
                        <i class="fa fa-facebook-official" style="font-size: 200%;color: blue;"></i>
                        <i class="fa fa-twitter-square" style="font-size: 200%;color: lightskyblue;"></i>
                        <i class="fa fa-google-plus-official" style="font-size: 200%;color: red;"></i>
                        
                        <p><b>Copyright &COPY; 2018. All Right Reserved.</b></p>
                    </div>
                </div>
            </div>
        </footer>
        
        
        <!-- Javascripts Added to the web page -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
    </body>
</html>
