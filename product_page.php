<?php
    session_start();
    include('connection.php');
    include('product_php.php');
    $username= $_SESSION['userId'];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>HairHub | Product </title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- End of Head section -->

    <style>
        .nav-link active
        {
            color: grey;
        }
    </style>

    <!-- Body Starts -->
    <body>
        <div id="main-content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12">
                        <ul class="nav nav-tabs" style="background-color: #343a40;color: grey;">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#all" style="color: grey;">All</a>
                            </li>
                            
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#new" style="color: grey;">New Arrivals</a>
                            </li>
                            
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#weavon" style="color: grey;">Weavons</a>
                            </li>
                            
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#attachment" style="color: grey;">Attachments</a>
                            </li>
                 
                            <ul class="nav nav-tabs ml-auto">
                                <li class="nav-item ">
                                    <a class="nav-link " href="view_cart.php" style="color: white;font-size: 100%;"><i  class="fa fa-shopping-cart">&nbsp;View Cart</i></a>
                                </li>
                            </ul>
                            </ul>
                            
                        
                        <div class="tab-content">
                            <div id="all" class="container tab-pane active"><br>
                                <!--ALL-->
                                <h4>ALL HAIR PRODUCTS</h4>
                                <hr style="background-color:red;"/>
                                <div class="container">
                                    <div class="row" style="margin-bottom:50px;">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <img src="img/beauty-casual-curly-794064.jpg" class="img-fluid" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-beauty-model-1234901.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";
                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                            ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">BUY</button>
                                            
                                            <div class="modal" id="myModal">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-beauty-model-1234901.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 1>1</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-close-up-773371.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                            ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2">BUY</button>
                                            
                                            <div class="modal" id="myModal2">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-close-up-773371.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 2>2</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/beautiful-blue-face-953266.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 2,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                                ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal3">BUY</button>
                                            
                                            <div class="modal" id="myModal3">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/beautiful-blue-face-953266.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 2,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 3>3</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!--new row-->
                                    <div class="row" style="margin-top:50px;margin-bottom: 30px;">
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-beauty-1006227.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 3,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                                ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal4">BUY</button>
                                            
                                            <div class="modal" id="myModal4">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-beauty-1006227.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 3,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 4>4</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>     
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/attractive-beautiful-beautiful-girl-713527.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 4,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                                ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal5">BUY</button>
                                            
                                            <div class="modal" id="myModal5">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/attractive-beautiful-beautiful-girl-713527.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 4,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 5>5</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>     
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-business-324030.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 5,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                                ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal6">BUY</button>
                                            
                                            <div class="modal" id="myModal6">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-business-324030.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 5,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 6>6 </option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>     
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            <!--NEW-->
                            <div id="new" class="container tab-pane fade"><br>
                                <h4>NEW ARRIVALS</h4>
                                <hr style="background-color:red;"/>
                                    <div class="container">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-beauty-model-1234901.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";
                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                            ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal7">BUY</button>
                                            
                                            <div class="modal" id="myModal7">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-beauty-model-1234901.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 1>1</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-close-up-773371.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                            ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal8">BUY</button>
                                            
                                            <div class="modal" id="myModal8">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-close-up-773371.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 2>2</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/beautiful-blue-face-953266.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 2,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                                ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal9">BUY</button>
                                            
                                            <div class="modal" id="myModal9">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/beautiful-blue-face-953266.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 2,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 3>3</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--WEAVON-->
                            <div id="weavon" class="container tab-pane fade"><br>
                                <h4>WEAVONS</h4>
                                <hr style="background-color:red;"/>
                                    <div class="container">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-beauty-model-1234901.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";
                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                            ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal10">BUY</button>
                                            
                                            <div class="modal" id="myModal10">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-beauty-model-1234901.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 1>1</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/beautiful-blue-face-953266.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 2,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                            ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal11">BUY</button>
                                            
                                            <div class="modal" id="myModal11">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/beautiful-blue-face-953266.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 2,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 3>3</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-business-324030.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 5,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                                ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal12">BUY</button>
                                            
                                            <div class="modal" id="myModal12">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-business-324030.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 5,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 6>6</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            <!--ATTACHMENT-->
                            <div id="attachment" class="container tab-pane fade"><br>
                                <h4>ATTACHMENTS</h4>
                                <hr style="background-color:red;"/>
                                    <div class="container">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-close-up-773371.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";
                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                            ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal13">BUY</button>
                                            
                                            <div class="modal" id="myModal13">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-close-up-773371.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 1,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 2>2</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/adult-beautiful-beauty-1006227.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 3,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                            ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal14">BUY</button>
                                            
                                            <div class="modal" id="myModal14">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/adult-beautiful-beauty-1006227.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 3,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 4>4</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 text-center">
                                            <img src="img/attractive-beautiful-beautiful-girl-713527.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                            <?php 
                                                $sql = "SELECT name,price FROM product_detail options LIMIT 4,1";
                                                $result = mysqli_query($conn, $sql);
                                                if(mysqli_num_rows($result) > 0)
                                                {  
                                                    while($row = mysqli_fetch_assoc($result))
                                                    {
                                                        echo "".$row['name']."<br>";
                                                        echo "₦".$row['price']."<br>";

                                                    }
                                                }
                                                else
                                                {
                                                    echo "0 results";
                                                }
                                                ?>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal15">BUY</button>
                                            
                                            <div class="modal" id="myModal15">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ADD TO SHOPPING CART</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        
                                                        <div class="modal-body text-center">
                                                            <img src="img/attractive-beautiful-beautiful-girl-713527.jpg" class="img-thumbnail img-responsive" alt="hair1" />
                                                            <?php 
                                                                $sql = "SELECT name,price FROM product_detail options LIMIT 4,1";
                                                                $result = mysqli_query($conn, $sql);
                                                                if(mysqli_num_rows($result) > 0)
                                                                {  
                                                                    while($row = mysqli_fetch_assoc($result))
                                                                    {
                                                                        echo "".$row['name']."<br>";
                                                                        echo "₦".$row['price']."<br>";
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo "0 results";
                                                                }
                                                            ?>
                                                            <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                                                                <div class="form-group">
                                                                    <label for="productid">Product_ID: </label>
                                                                    <select class="form-control " id="id" name="id" required="">
                                                                        <option selected value="">Select Product Id</option>
                                                                        <option value = 5>5</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="quantity">Quanity: </label>
                                                                    <input type="number" class="form-control" name="quantity" id="quantity" value="0" min="1" max="30"/>
                                                                </div>
                                                                
                                                                <button type="submit" class="btn btn-default" name="add" value="add">ADD</button>
                                                            </form>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>               
                            
                            <!--END OF  TABS-->
                        </div>
                </div>
            
        </div>
        
         <!-- Additional Javascripts added to webpage -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
    </body>
</html>
