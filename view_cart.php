<?php
    session_start();
    include('connection.php'); 
    $username= $_SESSION['userId'];
?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>HairHub | View Cart </title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- End of Head section -->
    
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            border: 1px solid black;
        }
        th {
            height: 50px;
            text-align: center;
            background-color: #c99695;
            border: 1px solid black;
            
        }
        tr:hover{
            background-color: #f5f5f5;
        }
        td{
            text-align: center;
            vertical-align: bottom;
            height: 50px;
            border: 1px solid black;
        }
        tr,td{
            padding: 15px;
        }
    </style>
    <!-- Body Starts -->
    
    <body>
        <ol class="breadcrumb" style="background-color: #343a40; color: white;">
            <li class="breadcrumb-item active">View Cart</li>
            <li class="breadcrumb-item">Proceed to Checkout</li>
        </ol>
        
        <!-- Main Content -->
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <h5 style="margin-bottom: 10px;"><b>CART ORDERS</b></h5>
                    <hr style="background-color:red;"/>
                    <?php 
                        echo "<b>Hi ".$_SESSION['userId']." ,</b>";
                    ?>
                    
                    <p>Listed below is a table of your cart orders. Kindly click the <em style="color:blue;"><b>CONFIRM</b></em> button to proceed to checkout.</p>
                    
                    <?php    
                    
                        $sql = "SELECT (cart_items.quantity * cart_items.price) AS total, cart_items.cart_id, cart_items.quantity, cart_items.price, product_detail.product_id, product_detail.name FROM cart_items, product_detail WHERE cart_items.product_id = product_detail.product_id AND cart_items.username = '$username'";
                        $result = mysqli_query($conn, $sql);
                        
                    ?>
                    <?php
                        $sqlb = "SELECT SUM(quantity * price) as alltotal FROM cart_items WHERE cart_items.username = '$username'";
                        $resultb = mysqli_query($conn, $sqlb);
                        $row = mysqli_fetch_assoc($resultb);
                        $sum = $row['alltotal'];
                        ?>
                    
                    <table><tr><th>CART ID</th><th>PRODUCT ID</th><th>PRODUCT NAME</th><th>QUANTITY</th><th>PRODUCT PRICE</th><th>TOTOAL EACH PRICE</th></tr>
                        <?php
                        if(mysqli_num_rows($result) > 0)
                        {  
                            
                            while($row = mysqli_fetch_assoc($result))
                            {           
                             ?>
                        
                             <tr>
                                <td><?php echo $row['cart_id']; ?></td>
                                <td><?php echo $row['product_id']; ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $row['quantity']; ?></td>
                                <td><?php echo $row['price']; ?></td>
                                <td><?php echo $row['total']; ?></td>
                                <td><input type="button" onClick="deleteme(<?php echo $row['cart_id']; ?>)" class="btn btn-danger" name="Delete" value="Delete"></td>   
                            </tr>    
                            <?php
                            }           
                            }
                            else 
                                {
                            ?>
                            <tr>
                                <th>theres  no  data found!!!</th>
                            </tr>
                            <?php
                                }
                                ?>
                            </table>
                    
                            <h5 style="text-align:right;margin-top: 20px;"><b>TOTAL AMOUNT: ₦<?php echo $sum ?></b></h5>
                        
                    
                            <a onclick="myFunction()" class="btn btn-primary" href="#">CONFRIM CART ITEM</a>
                           
                </div>
            </div>
        </div>
        <!-- End of Main Content -->

        <!-- Additional Javascripts added to webpage -->
       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
        <script lang="javascript">
            function deleteme(delid)
            {
                if(confirm("Do you want to Delete!"))
                {
                    window.location.href='delete.php?del_id=' +delid+'';
                    return true;
                }
            }
        </script>
        
        <script type="text/javascript">
            function myFunction() {
                var x;
                if (confirm("Are you sure?") == true) {
                    window.alert("THANKS FOR ORDERING FROM HAIRHUB!");
                    $.get("email_notify.php");
                    $.get("sms_notify.php");
                    return false;
                    x = "You pressed OK!";
                    
                } else {
                    x = "You pressed Cancel!";
                }
                document.getElementById("demo").innerHTML = x;
            }
            </script>
    </body>
    <!-- end of body -->
</html>
