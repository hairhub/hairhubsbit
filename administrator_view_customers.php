<?php
session_start();
include('connection.php'); 
$username= $_SESSION['adminId'];
?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>HairHub | View Customers </title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- End of Head section -->
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            border: 1px solid black;
        }
        th {
            height: 50px;
            text-align: center;
            background-color: #c99695;
            border: 1px solid black;
            
        }
        tr:hover{
            background-color: #f5f5f5;
        }
        td{
            text-align: center;
            vertical-align: bottom;
            height: 50px;
            border: 1px solid black;
        }
        tr,td{
            padding: 15px;
        }
    </style>
    <!-- Body Starts -->
    
    <body>
        <ol class="breadcrumb" style="background-color: #343a40; color: white;">
            <li class="breadcrumb-item active">View Customers</li>
        </ol>
        
        <!-- Main Content -->
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <h5 style="margin-bottom: 10px;"><b>CUSTOMER LIST</b></h5>
                    <hr style="background-color:red;"/>
                    <?php 
                        echo "<b>Hi ".$_SESSION['adminId']." ,</b>";
                    ?>
                    
                    <p>Listed below is a table of the registered customers.</p>
                    
                    <?php    
                    
                        $sql = "SELECT user_id,fullname,username,email_address,phone_number,home_address FROM customer_register";
                        $result = mysqli_query($conn, $sql);
                        
                    ?>
                    
                    <table><tr><th>USER ID</th><th>FULLNAME</th><th>USERNAME</th><th>EMAIL ADDRESS</th><th>PHONE NUMBER</th><th>HOME ADDRESS</th></tr>
                        <?php
                        if(mysqli_num_rows($result) > 0)
                        {  
                            
                            while($row = mysqli_fetch_assoc($result))
                            {           
                             ?>
                        
                             <tr>
                                <td><?php echo $row['user_id']; ?></td>
                                <td><?php echo $row['fullname']; ?></td>
                                <td><?php echo $row['username']; ?></td>
                                <td><?php echo $row['email_address']; ?></td>
                                <td><?php echo $row['phone_number']; ?></td>
                                <td><?php echo $row['home_address']; ?></td>
                            </tr>    
                            <?php
                            }           
                            }
                            else 
                                {
                            ?>
                            <tr>
                                <th>theres  no  data found!!!</th>
                            </tr>
                            <?php
                                }
                                ?>
                            </table>
                    
                </div>
            </div>
        </div>
        <!-- End of Main Content -->

        <!-- Additional Javascripts added to webpage -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
       
    </body>
    <!-- end of body -->
</html>
