<?php
session_start();
include('connection.php'); 
$username= $_SESSION['userId'];

?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>HairHub | Email Notification</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- End of Head section -->
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            border: 1px solid black;
        }
        th {
            height: 50px;
            text-align: center;
            background-color: #c99695;
            border: 1px solid black;
            
        }
        tr:hover{
            background-color: #f5f5f5;
        }
        td{
            text-align: center;
            vertical-align: bottom;
            height: 50px;
            border: 1px solid black;
        }
        tr,td{
            padding: 15px;
        }
    </style>
    <body>
        <?php
            $sql = "SELECT fullname,email_address FROM customer_register WHERE username = '$username'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result) > 0)                    //execute statements if the number of rows in the table is more than 0
                    {
                        while($row = mysqli_fetch_assoc($result))       //execute the statments while they are being fetched.
                        {   
                            $email_address = $row['email_address'];
                            $fullname = $row['fullname'];

                        }
                    }
                    else
                    {
                        echo '<script>window.alert("Invalid Username or Password. Please Try Again");</script>';
                    }

            $sql2 = "SELECT (cart_items.quantity * cart_items.price) AS total, cart_items.cart_id, cart_items.quantity, cart_items.price, product_detail.name FROM cart_items, product_detail WHERE cart_items.product_id = product_detail.product_id AND cart_items.username = '$username'";
            $result2 = mysqli_query($conn, $sql2);

            $sqlb = "SELECT SUM(quantity * price) as alltotal FROM cart_items WHERE cart_items.username = '$username'";
            $resultb = mysqli_query($conn, $sqlb);
            $row = mysqli_fetch_assoc($resultb);
            $sum = $row['alltotal'];

            ?>
            <?php
            
                // Import PHPMailer classes into the global namespace
                // These must be at the top of your script, not inside a function
                use PHPMailer\PHPMailer\PHPMailer;
                use PHPMailer\PHPMailer\Exception;

                //Load Composer's autoloader
                require 'vendor/autoload.php';

                
                //Create a new PHPMailer instance
                $mail = new PHPMailer;
                //Tell PHPMailer to use SMTP
                $mail->isSMTP();
                //Enable SMTP debugging
                // 0 = off (for production use)
                // 1 = client messages
                // 2 = client and server messages
                $mail->SMTPDebug = 2;
                //Set the hostname of the mail server
                $mail->Host = 'smtp.gmail.com';
                // use
                // $mail->Host = gethostbyname('smtp.gmail.com');
                // if your network does not support SMTP over IPv6
                // //Set the encryption system to use - ssl (deprecated) or tls
                $mail->SMTPSecure = 'ssl';
                //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
                $mail->Port = 465;
                //Whether to use SMTP authentication
                $mail->SMTPAuth = true;
                //Username to use for SMTP authentication - use full email address for gmail
                $mail->Username = "elizabeth.adeniran98@gmail.com";
                //Password to use for SMTP authentication
                $mail->Password = "Elizabeth@18";
                //Set who the message is to be sent from
                $mail->From = 'elizabeth.adeniran98@gmail.com'; 
                $mail->FromName = 'Elizabeth Adeniran';
                //Set an alternative reply-to address
                $mail->addReplyTo('elizabeth.adeniran98@gmail.com', 'Elizabeth Adeniran');
                //Set who the message is to be sent to
                $mail->addAddress($email_address, $fullname);
                // Set email format to HTML
                $mail->isHTML(true);
                //Set the subject line
                $mail->Subject = 'Email Notification for Shopping Cart Order From HairHub';
                //Read an HTML message body from an external file, convert referenced images to embedded,
                $body = '<table><tr><th>CART ID</th><th>PRODUCT NAME</th><th>QUANTITY</th><th>PRODUCT PRICE</th><th>TOTAL PRICE(FOR EACH)</th></tr>';
                    if(mysqli_num_rows($result2) > 0)
                    {
                        while($row = mysqli_fetch_assoc($result2))
                        {
                            $body .= "<tr><td>".$row['cart_id']."</td><td>".$row['name']."</td><td>".$row['quantity']."</td><td>".$row['price']."</td><td>".$row['total']."</td></tr>"; 

                        }
                    }
                    else 
                        {
                        echo "theres  no  data found!!!";
                        }

                $mail->Body = $body.'</table>';
                $mail->Body = $body."TOTAL CART AMOUNT IS: "."₦".$sum;

                //$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
                //Replace the plain text body with one created manually
                $mail->AltBody = 'This is a plain-text message body';
                //send the message, check for errors
                if (!$mail->send()) {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                } else {
                    echo "Message sent!";

                }

            ?>

       
  </body>
    <!-- end of body -->
   
</html>