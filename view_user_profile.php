<?php
    session_start();
    include('connection.php'); 
    $username= $_SESSION['userId'];
?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>HairHub | View User Profile </title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- End of Head section -->
    
    <!-- Body Starts -->
    <body>
        <ol class="breadcrumb" style="background-color: #343a40; color: white;">
            <li class="breadcrumb-item active">View Profile</li>
            <li class="breadcrumb-item">Update Profile</li>
        </ol>
        
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-12">
                    <h3 style="margin-left:20px;margin-bottom: 10px;"><b>BASIC DETAILS</b></h3>
                    <hr style="background-color:red;"/>
                    <?php           
                        $sql = "SELECT * FROM customer_register WHERE username = '$username'";
                        $result = mysqli_query($conn, $sql);
                        if(mysqli_num_rows($result) > 0)
                        {  
                            while($row = mysqli_fetch_assoc($result))
                            {
                                echo "<h6><i class='fa fa-users' style='font-size: 150%;margin-left:20px;margin-bottom:30px;'><b>&nbsp; Full Name: </b></i>"." ".$row['fullname']. "</h6>";
                                echo "<h6><i  class='fa fa-user' style='font-size: 150%;margin-left:20px;margin-bottom:30px;'><b>&nbsp; User Name: </b></i>"." ". $row['username']. "</h6>";
                                echo "<h6><i  class='fa fa-envelope' style='font-size: 150%;margin-left:20px;margin-bottom:20px;'><b>&nbsp; Email Address: </b></i>"." ". $row['email_address']. "</h6>";
                                echo "<h6><i  class='fa fa-phone' style='font-size: 150%;margin-left:20px;margin-bottom:30px;'><b>&nbsp; Phone Number: </b></i>"." ". $row['phone_number']. "</h6>";
                                echo "<h6><i  class='fa fa-map-marker' style='font-size: 150%;margin-left:20px;margin-bottom:30px;'><b>&nbsp; Home Address: </b></i>"." ". $row['home_address']. "</h6>";                   
                                
                            }
                        }
                        else
                        {
                            echo "0 results";
                        }
                    ?>

                    <button class="btn btn-danger" style="margin-left: 100px;margin-top: 10px;"><a href="update_user_profile.php" style="color: white;"><b>UPDATE</b></a></button>
                    
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <img src="img/186703_12827908_2441458_a2a821eb_image.jpg" width="100%"/>
                </div>
            </div>
        </div>
        <!-- End of Main Content -->

        <!-- Additional Javascripts added to webpage -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
    </body>
    <!-- end of body -->
</html>
