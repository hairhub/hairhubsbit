<?php 
session_start();
include('connection.php'); 
$username= $_SESSION['adminId'];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>HairHub| Add Customer</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
        <link href="font awesome/font/css/font-awesome.css" type="text/css" rel="stylesheet" />
   
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php 
        $errors = array();
        if (isset($_POST['add']))          
    {
        //assign variables to the name of the input boxes the users click
        
        $fname = $_POST['fname'];
        
        $uname = $_POST['uname'];
        
        $email = $_POST['email'];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))         //To check that the right email format is inputted
        {
            array_push($errors, '<script>window.alert("Invalid email format");</script>');
        }
         
        $phone =  $_POST['phone'];
        
        $address = $_POST['address'];
        
        $pass = $_POST['password'];
        
        $pass2 = $_POST['password2'];
        
        //condtion to check if the two passwords match
        if ($pass != $pass2)
        {
            array_push($errors, '<script>window.alert("The two passwords do not match");</script>');
        }
           
        
        //SQL queries to insert the inputted values into the database
        if (count($errors) == 0)
        {       
            $sql = "INSERT INTO customer_register(fullname,username,email_address,phone_number,home_address,password) VALUES ('$fname','$uname','$email','$phone','$address',md5('$pass'))";
                if (mysqli_query($conn, $sql)) 
                {   
                    echo '<script>window.alert("Thanks for adding a new customer");</script>';
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }  
                
            $sql2 = "INSERT INTO customer_login(username, password) SELECT customer_register.username, customer_register.password FROM customer_register ";
                if (mysqli_query($conn, $sql2))
                {
                    echo '<script>window.alert("Thanks for Registering with Us");</script>';
                } 
                else 
                {
                    echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
                }
                
            $sql3 = "INSERT INTO shopping_cart(user_id,username) SELECT customer_register.user_id,customer_register.username FROM customer_register";
                if (mysqli_query($conn, $sql3))
                {
                    echo '<script>window.alert("Thanks for Registering with Us");</script>';
                } 
                else 
                {
                    echo "Error: " . $sql3 . "<br>" . mysqli_error($conn);
                }
                
               // header('Location: ');
            }
        }
        
        function test_input($data)
        {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
        }
        ?>
        
    </head>
    <!-- End of Head -->
    
    
    <body>
        <ol class="breadcrumb" style="background-color: #343a40; color: white;">
            <li class="breadcrumb-item">View Profile</li>
            <li class="breadcrumb-item active">Update Profile</li>
        </ol>
        
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-12">
                    <h3 style="margin-left:20px;margin-bottom: 10px;"><b>UPDATE PROFILE</b></h3>
                    <hr style="background-color:red;"/>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                        <?php include('errors.php'); ?>
                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="fname"><i class="fa fa-user" style="font-size: 120%;"><b> &nbsp;Full Name: </b></i></label>
                            <input type="text" name="fname" class="form-control" id="fname" required="" />
                        </div>
                        
                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="uname"><i class="fa fa-user" style="font-size: 120%;"><b> &nbsp;Username: </b></i></label>
                            <input type="text" name="uname" class="form-control" id="uname" required="" />
                        </div>

                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="email"><i class="fa fa-envelope" style="font-size: 120%;"><b> &nbsp;Email Address: </b></i></label>
                            <input type="email" class="form-control" id="email" name="email" required="">
                        </div>

                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="phone"><i class="fa fa-phone" style="font-size: 120%;"><b> &nbsp;Phone Number: </b></i></label>
                            <input type="tel" class="form-control" id="phone" name="phone" required="">
                        </div>

                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="address"><i class="fa fa-home" style="font-size: 120%;"><b> &nbsp;Home Address: </b></i></label>
                            <textarea rows="5"  name="address" id="address" required="" class="form-control" required=""></textarea>
                        </div>

                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="password"><i class="fa fa-lock" style="font-size: 120%;"><b> &nbsp;Password: </b></i></label>
                            <input type="password" class="form-control" id="password" name="password" required="">
                        </div>
                        <div class="form-group" style="margin-left:40px;margin-bottom: 10px;margin-right: 20px;">
                            <label for="password2"><i class="fa fa-lock" style="font-size: 120%;"><b> &nbsp;Confirm Password: </b></i></label>
                            <input type="password" class="form-control" id="password2" name="password2" required=""/>
                        </div>   
  
                        <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" value="">Agree to Terms and Conditions
                                        </label>
                                    </div>
                        
                        <button type="submit" name="add" class="btn btn-primary" value="add" style="margin-top:5px;margin-bottom: 15px;margin-left: 40px;"><b>ADD</b></button>
                        
                         </form>
                   
                </div>
                <div class="col-md-4 col-sm-4 col-12">
                    <img src="img/186703_12827908_2441458_a2a821eb_image.jpg" width="100%"/>
                </div>
            </div>
        </div>
               
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
    </body>
</html>